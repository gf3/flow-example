type ModelRecord = {
  entity: string;
  delete(instance: Object): Promise;
  fetch(id: string): Promise;
  fetchAll(params: Object): Promise;
  save(instance: Object): Promise;
};

type ModelInstanceRecord = {
  delete(instance: Object): Promise;
  fetch(id: string): Promise;
  fetchAll(params: Object): Promise;
  save(instance: Object): Promise;
};
