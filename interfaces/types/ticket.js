type TicketRecord = {
  id?: string;
  access_keys?: Array<string>;
  cost_items: Array<Object>;
  discount_code?: string;
  event_id: string;
  host_fields: Array<Object>;
  provider: string;
  token: string;
};
