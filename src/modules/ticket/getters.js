/**
 * @flow weak
 */

import { toImmutable } from 'nuclear-js';
import { createEntityMapGetter, createByIdGetter } from '../rest-api';
import model from './model';

export const entityMap = createEntityMapGetter(model);
export const byId = createByIdGetter(model);

export const ticketList = [
  entityMap,
  map => map.toList()
];

