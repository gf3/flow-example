/**
 * @flow
 */

import { createActions } from '../rest-api';
import model from './model';

export default createActions(model);
