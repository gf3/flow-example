/**
 * @flow
 */

import { toJS } from 'nuclear-js';
import Config from 'config';
import request from 'xhttp';

const ENTITY = 'ticket';

export default {
  entity: ENTITY,

  /**
   * Save ticket.
   */
  save(instance: TicketRecord): Promise {
    const method = instance.id ? 'PUT' : 'POST';

    return request({
      data: { ticket: instance },
      type: 'json',
      url: `${Config.apiBaseUrl}/tickets/${instance.id}`,
      method
    }).then(response => response.ticket);
  },

  /**
   * Fetch ticket.
   */
  fetch(id: string): Promise {
    return request({ url: `${Config.apiBaseUrl}/tickets/${id}` }).then(response => response.ticket);
  },

  /**
   * Fetch all tickets.
   */
  fetchAll(params: Object): Promise {
    return Promise.reject('Not implemented');
  },

  /**
   * Delete ticket.
   */
  delete(instance: TicketRecord): Promise {
    return Promise.reject('Not implemented');
  }
};
