/**
 * @flow
 */

import * as actions from './actions';
import * as getters from './getters';

export default {
  actions,
  getters
};

