/**
 * @flow
 */

import { toImmutable } from 'nuclear-js';
import flux from '../../flux';
import * as actions from './actions';
import restApiCache from './stores/rest-api-cache-store';
import createActions from './create-actions';

export { createActions };

flux.registerStores({ restApiCache });

/**
 * Protect entity map from being undefined.
 */
function entityMapGetter(entityMap: Map): Map {
  if (!entityMap) {
    return toImmutable({});
  }

  return entityMap;
}

/**
 * Creates a getter to the restApiCache store for a particular entity This
 * decouples the implementation details of the RestApi module's caching to
 * consumers of the cached data.
 */
export function createEntityMapGetter(model: ModelRecord): Array<mixed> {
  return [
    ['restApiCache', model.entity],
    entityMapGetter
  ];
}

/**
 * Factory that returns a getter that looks up the entity in the cache by ID.
 */
export function createByIdGetter(model: ModelRecord): (id: string) => Array<mixed> {
  return function(id) {
    return ['restApiCache', model.entity, id];
  };
}

export default {
  actions,
  createActions,
  createEntityMapGetter,
  createByIdGetter
};
