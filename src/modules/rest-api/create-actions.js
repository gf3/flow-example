/**
 * @flow
 */

import flux from '../../flux';
import actions from './action-types';

/**
 * Factory for generating handler for successful API actions.
 */
function success(action: string): (model: ModelRecord, params: mixed, result: Object) => Object {
  /**
   * Handler for successful API action; dispatches flux action to store the
   * result in the api cache.
   */
  return function actionSuccess(model, params, result) {
    flux.dispatch(action, { model, params, result });
    return result;
  };
}

/**
 * Factory for generating handler for successful API actions.
 */
function failure(action: string): (model: ModelRecord, params: mixed, result: Object) => Object {
  /**
   * Handler for failure API action; dispatches flux action to store the result
   * in the api cache.
   */
  return function actionFailure(model, params, reason) {
    flux.dispatch(action, { model, params, reason });
    return Promise.reject(reason);
  };
}

/**
 * Create API actions for a given entity.
 */
export default function createActions(model: ModelRecord): ModelInstanceRecord {
  return {
    fetch(...params) {
      flux.dispatch(actions.API_FETCH_START, {
        method: 'fetch',
        model,
        params
      });

      return model.fetch(...params).then(
        success(actions.API_FETCH_SUCCESS).bind(null, model, params),
        failure(actions.API_FETCH_FAIL).bind(null, model, params)
      );
    },

    fetchAll(...params) {
      flux.dispatch(actions.API_FETCH_START, {
        method: 'fetchAll',
        model,
        params
      });

      return model.fetchAll(...params).then(
        success(actions.API_FETCH_SUCCESS).bind(null, model, params),
        failure(actions.API_FETCH_FAIL).bind(null, model, params)
      );
    },

    save(...params) {
      flux.dispatch(actions.API_SAVE_START, {
        method: 'save',
        model,
        params
      });

      return model.save(...params).then(
        success(actions.API_SAVE_SUCCESS).bind(null, model, params),
        failure(actions.API_SAVE_FAIL).bind(null, model, params)
      );
    },

    delete(...params) {
      flux.dispatch(actions.API_DELETE_START, {
        method: 'delete',
        model,
        params
      });

      return model.delete(...params).then(
        success(actions.API_DELETE_SUCCESS).bind(null, model, params),
        failure(actions.API_DELETE_FAIL).bind(null, model, params)
      );
    }
  };
}
