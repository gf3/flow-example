/**
 * @flow
 */

import flux from '../../flux';
import actions from './action-types';

export function hydrate(data: Object): void {
  flux.dispatch(actions.API_HYDRATE, data);
}
