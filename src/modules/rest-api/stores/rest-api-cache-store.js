/**
 * @flow weak
 */

import Nuclear, { toImmutable } from 'nuclear-js';
import { singularize } from 'inflect';
import actions from '../action-types';


/**
 * Load data into store.
 *
 * @param {Immutable.Map} state
 * @param {{ model: Model, params, result: (Object|Array)}} payload
 *
 * @return {Immutable.Map}
 */
function loadData(state, { model, params, result }) {
  const { entity } = model;
  let data = result;

  if (!data) {
    return state;
  }

  if (!(data instanceof Array)) {
    data = [data];
  }

  return state.withMutations((state) => {
    data.forEach((entry) => state.setIn([entity, entry.id], toImmutable(entry)));
  });
}

/**
 * Remove data from store.
 *
 * Note: `params` is an argument *list*.
 *
 * @param {Immutable.Map} state
 * @param {{ model: Model, params, result: (Object|Array) }} payload
 *
 * @return {Immutable.Map}
 */
function removeData(state, { model, params, result }) {
  const { entity } = model;
  const { id } = params[0];

  return state.removeIn([entity, id]);
}

/**
 * Hydrate identity map from data.
 *
 * @param {Immutable.Map} state
 * @param {object} payload
 *
 * @return {Immutable.Map}
 */
function hydrateData(state, payload) {
  return state.withMutations((state) => {
    Object.keys(payload).forEach(key => {
      let data = payload[key];
      const entity = singularize(key);

      if (!(data instanceof Array)) {
        data = [data];
      }

      data.forEach(entry => state.setIn([entity, entry.id], toImmutable(entry)));
    });
  });
}


//-----------------------------------------------------------------------------
// Store
//-----------------------------------------------------------------------------

export default new Nuclear.Store({
  getInitialState() {
    return toImmutable({});
  },

  initialize() {
    this.on(actions.API_FETCH_SUCCESS, loadData);
    this.on(actions.API_SAVE_SUCCESS, loadData);
    this.on(actions.API_DELETE_SUCCESS, removeData);
    this.on(actions.API_HYDRATE, hydrateData);
  }
});
